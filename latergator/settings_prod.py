"""
Django settings for latergator project.
"""
from .settings import *
import os

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ['SECRET_KEY']
PASSWORD = os.environ['PG_PASSWORD']

ALLOWED_HOSTS = ['*']

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join('/djangoappmedia')

# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'latergator',
        'USER': 'gator',
        'PASSWORD': PASSWORD,
        'HOST': 'fabbricone.clwtqrb1woxk.us-east-2.rds.amazonaws.com',
        'PORT': '5432',
    },
}

SOCIAL_AUTH_APPLE_ID_SECRET = os.environ['SOCIAL_AUTH_APPLE_ID_SECRET']

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': 'debug.log',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'DEBUG',
            'propagate': True,
        },
        '': {
            'handlers': ['file'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}
