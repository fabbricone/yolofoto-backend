from .settings import *
from .settings_private import _SOCIAL_AUTH_APPLE_ID_SECRET

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-z27_&+l5r&bvg2jnl*^3o!psg78ep3hzum3(c#*t&=lsuh5ixc'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

DEFAULT_MEDIA_URL = 'media'
MEDIA_URL = '/' + DEFAULT_MEDIA_URL + '/'
MEDIA_ROOT = os.path.join(BASE_DIR, DEFAULT_MEDIA_URL)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'latergator',
        'USER': 'gator',
        'PASSWORD': 'whyd1d789',
        'HOST': 'localhost',
        'PORT': '5432',
    },
    'test': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'latergator_test',
        'USER': 'gator',
        'PASSWORD': 'whyd1d789',
        'HOST': 'localhost',
        'PORT': '5432',
    },
}

SOCIAL_AUTH_APPLE_ID_SECRET = _SOCIAL_AUTH_APPLE_ID_SECRET
