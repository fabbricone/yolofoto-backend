```shell
conda list \
    --explicit \
    > later-env.txt

conda env \
    create \
    --file later-env.txt
    gator
```

```shell
http \
    http://127.0.0.1:8000/api/token/ \
    username=lawlor \
    password=asdfasdf

http POST \
    http://127.0.0.1:8000/api/token/refresh/ \
    refresh=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTYxOTU2ODcwNiwianRpIjoiMmEyNzFjMzc1OGQ1NGZlYzg2OGEwNDdiNDFlZWEwNTIiLCJ1c2VyX2lkIjoxfQ.4RmShMn9CTWMSixK-05BUiFGBN-4dW4FIxe1s2dyH7U
```
