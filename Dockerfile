FROM phusion/baseimage:focal-1.0.0

RUN add-apt-repository ppa:deadsnakes/ppa

RUN apt-get update && \
    apt-get install --no-install-recommends --yes \
    nginx \
    python3.9 \
    python3-pip \
    build-essential \
    python3-dev \
    libpq-dev \
    libhdf5-dev \
    uwsgi \
    nano

RUN update-alternatives --install /usr/bin/python python /usr/bin/python3.9 10

COPY nginx/mysite_nginx.conf /etc/nginx/sites-available/

RUN ln -s /etc/nginx/sites-available/mysite_nginx.conf /etc/nginx/sites-enabled/

WORKDIR /django-app

COPY requirements.txt ./

RUN pip3 install -r requirements.txt

COPY . ./

ARG DJANGO_SETTINGS_MODULE
ARG SECRET_KEY
ARG PG_PASSWORD
ARG SOCIAL_AUTH_APPLE_ID_SECRET

RUN python3 manage.py collectstatic

RUN mkdir -p media/photos

CMD ["./deploy/wrapper_script.sh"]
