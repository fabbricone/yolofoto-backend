#!/bin/bash

# Start the first process
./deploy/nginx_process -D
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start nginx_process: $status"
  exit $status
fi

# Start the second process
./deploy/uwsgi_process -D
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start uwsgi_process: $status"
  exit $status
fi

# Naive check runs checks once a minute to see if either of the processes exited.
# This illustrates part of the heavy lifting you need to do if you want to run
# more than one service in a container. The container exits with an error
# if it detects that either of the processes has exited.
# Otherwise it loops forever, waking up every 60 seconds

i=0
while sleep 60; do
    ps aux |grep nginx_process |grep -q -v grep
    PROCESS_1_STATUS=$?
    ps aux |grep uwsgi_process |grep -q -v grep
    PROCESS_2_STATUS=$?
    # If the greps above find anything, they exit with 0 status
    # If they are not both 0, then something is wrong
    if [ $PROCESS_1_STATUS -ne 0 -o $PROCESS_2_STATUS -ne 0 ]; then
    echo "One of the processes has already exited."
    exit 1
    fi

    if [[ "$i" -eq 360 ]]; then
        nginx -s reload
        nginx -g "daemon off;"
        i=0
    fi
    i=$((i+1))
done