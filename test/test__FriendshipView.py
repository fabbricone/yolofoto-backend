from collections import OrderedDict

from rest_framework import status
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import force_authenticate

from rest_api import views_friendship
from rest_api.models import Friendship, FriendshipStatus
from test.TestBase import TestBase

AN_INVALID_CHOICE = 'an_invalid_choice'


class FriendshipViewTestCase(TestBase):

    def setUp(self):
        self.friendships_path = "/friendships/"
        self.friends_path = "/friends/"
        self.friends_requested_path = "/friends_requested/"
        super().setUp()

        self.request_post_friendships = self.factory.post(self.friendships_path)
        self.request_patch_friendships = self.factory.patch(self.friendships_path)
        self.request_find_friends = self.factory.get(self.friendships_path)
        self.request_get_friends = self.factory.get(self.friends_path, {
            'status': FriendshipStatus.ACCEPTED.value
        })
        self.request_list_friends_requested = self.factory.get(self.friendships_path, {
            'status': FriendshipStatus.REQUESTED.value
        })
        self.request_get_friends_bad_param = self.factory.get(self.friends_path, {
            'status': 'total_junk'
        })

        self.response_no_data = {
            'from_user': [ErrorDetail(string='This field is required.', code='required')],
            'to_user': [ErrorDetail(string='This field is required.', code='required')]
        }
        self.response_success_post = {
            'from_user': self.user1.id,
            'to_user': self.user2.id,
            'status': 'requested'
        }
        self.response_success_patch = {
            'from_user': self.user1.id,
            'to_user': self.user3.id,
            'status': 'accepted'
        }

        self.friendship1 = Friendship.objects.create(from_user=self.user1, to_user=self.user3, status=FriendshipStatus.ACCEPTED.value)
        self.friendship2 = Friendship.objects.create(from_user=self.user1, to_user=self.user4, status=FriendshipStatus.ACCEPTED.value)
        self.friendship3 = Friendship.objects.create(from_user=self.user1, to_user=self.user5, status=FriendshipStatus.ACCEPTED.value)
        self.friendship4 = Friendship.objects.create(from_user=self.user1, to_user=self.user6, status=FriendshipStatus.ACCEPTED.value)

        self.friendship5 = Friendship.objects.create(from_user=self.user1, to_user=self.user7, status=FriendshipStatus.REQUESTED.value)
        self.friendship6 = Friendship.objects.create(from_user=self.user1, to_user=self.user9, status=FriendshipStatus.DENIED.value)

        self.num_friendships_before = Friendship.objects.count()

    def tearDown(self) -> None:
        super().tearDown()

    def test_request_friendship_success(self):
        request = self.factory.post(self.friendships_path, data={
            "to_user": self.user2.id,
        })
        force_authenticate(request, user=self.user1)

        response = views_friendship.FriendshipList.as_view()(request)

        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self._assert_friendship_response(self.response_success_post, response.data)
        self.assertEqual(self.num_friendships_before+1, Friendship.objects.count())

    def test_request_friendship_bad_data(self):
        force_authenticate(self.request_post_friendships, user=self.user1)

        response = views_friendship.FriendshipList.as_view()(self.request_post_friendships)

        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertEqual(self.response_no_data, response.data)

    def test_request_friendship_not_signed_in(self):
        self._assert_when_not_signed_in(
            lambda: views_friendship.FriendshipList.as_view()(self.request_post_friendships))


    # this also tests denying a friendship
    def test_accept_friendship_success(self):
        request = self.factory.patch(self.friendships_path+str(self.friendship1.id), data={
            "status": 'accepted',
        })
        force_authenticate(request, user=self.user1)

        response = views_friendship.FriendshipDetail.as_view()(request, self.friendship1.id)

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self._assert_friendship_response(self.response_success_patch, response.data)

    def test_accept_friendship_bad_data(self):
        request = self.factory.patch(self.friendships_path+str(self.friendship1.id), data={
            "status": ('%s' % AN_INVALID_CHOICE),
        })
        force_authenticate(request, user=self.user1)

        response = views_friendship.FriendshipDetail.as_view()(request, self.friendship1.id)

        response_data = {
            'status': [ErrorDetail(string=('"%s" is not a valid choice.' % AN_INVALID_CHOICE), code='invalid_choice')]
        }
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self._assert_friendship_response(response_data, response.data)

    def _assert_friendship_response(self, response_success, data):
        for key in response_success.keys():
            self.assertEqual(response_success.get(key), data.get(key))

    def test_accept_friendship_denied_access(self):
        view_call = lambda: views_friendship.FriendshipDetail.as_view()(self.request_patch_friendships, self.friendship1.id)
        self._assert_not_allowed(self.request_patch_friendships, view_call)

    def test_accept_friendship_not_signed_in(self):
        self._assert_when_not_signed_in(
            lambda: views_friendship.FriendshipDetail.as_view()(self.request_patch_friendships, self.friendship1.id))

    # todo
    # def test_delete_friendship_success(self):
    #     pass
    #
    # def test_delete_friendship_denied_access(self):
    #     pass
    #
    # def test_delete_friendship_not_signed_in(self):
    #     pass

    def test_find_friends_success_all(self):
        force_authenticate(self.request_find_friends, user=self.user1)

        response = views_friendship.FriendshipList.as_view()(self.request_find_friends)

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        response_data = response.data

        names = [user.get('name') for user in response_data]
        self.assertTrue(self.user1.username not in names)
        self.assertTrue(self.user3.username not in names)
        self.assertTrue(self.user4.username not in names)
        self.assertTrue(self.user5.username not in names)
        self.assertTrue(self.user9.username not in names)
        self.assertEqual(5, len(response_data))
        expected_reponse = self._get_expected_friendship_response_for_user(self.user8, friendship=None)
        actual_reponse = dict(response_data[0])
        self.assertEqual(expected_reponse, actual_reponse)

    def test_find_friends_success_prefix(self):
        request = self.factory.get(self.request_find_friends, {
            'prefix': 'jo'
        })
        force_authenticate(request, user=self.user1)

        response = views_friendship.FriendshipList.as_view()(request)

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        response_data = response.data
        names = [user.get('name') for user in response_data]
        self.assertTrue(self.user6.username not in names)
        self.assertTrue(self.user9.username not in names)
        self.assertEqual(2, len(response_data))
        self.assertEqual(self._get_expected_friendship_response_for_user(self.user11), response_data[0])

    def test_find_friends_success_prefix_not_current_user(self):
        request = self.factory.get(self.request_find_friends, {
            'prefix': 'l'
        })
        force_authenticate(request, user=self.user1)

        response = views_friendship.FriendshipList.as_view()(request)

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        response_data = response.data

        names = [user.get('name') for user in response_data]
        self.assertTrue(self.user1.username not in names)
        self.assertEqual(0, len(response_data))

    def test_find_friends_not_signed_in(self):
        self._assert_when_not_signed_in(
            lambda: views_friendship.FriendshipList.as_view()(self.request_find_friends))

    def test_get_friends_success(self):
        force_authenticate(self.request_get_friends, user=self.user1)

        response = views_friendship.FriendList.as_view()(self.request_get_friends)

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(4, len(response.data))

        response_data = response.data
        self.assertEqual(self._get_expected_friends_response_for_user(self.user5), response_data[0])
        self.assertEqual(self._get_expected_friends_response_for_user(self.user3), response_data[1])
        self.assertEqual(self._get_expected_friends_response_for_user(self.user4), response_data[2])
        self.assertEqual(self._get_expected_friends_response_for_user(self.user6), response_data[3])

    # todo do we deny someone access when no object is requested?
    # def test_get_friends_friendship_denied_access(self):
    #     view_call = lambda: views_friendship.FriendshipList.as_view()(self.request_get_friends)
    #     self._assert_not_allowed(self.request_get_friends, view_call)

    def test_get_friends_not_signed_in(self):
        self._assert_when_not_signed_in(
            lambda: views_friendship.FriendList.as_view()(self.request_get_friends))

    def test_list_friends_requested(self):
        force_authenticate(self.request_list_friends_requested, user=self.user1)

        response = views_friendship.FriendshipList.as_view()(self.request_list_friends_requested)

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(1, len(response.data))

        response_data = response.data
        friendship = {
            'id': 71,
            'from_user': self.user1.id,
            'to_user': self.user7.id,
            'status': 'requested'
        }
        self.assertEqual(self._get_expected_friendship_response_for_user(self.user7, friendship), response_data[0])

    def test_list_friends_requested_not_signed_in(self):
        self._assert_when_not_signed_in(
            lambda: views_friendship.FriendshipList.as_view()(self.request_list_friends_requested))

    def test_get_friends_bad_param(self):
        force_authenticate(self.request_get_friends_bad_param, user=self.user1)

        response = views_friendship.FriendList.as_view()(self.request_get_friends_bad_param)

        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertEqual(None, response.data)

    def _get_expected_friends_response_for_user(self, user):
        return OrderedDict([
            ('url', 'http://testserver/users/{}/'.format(user.id)),
            ('id', user.id),
            ('name', user.username)
        ])

    def _get_expected_friendship_response_for_user(self, user, friendship=None):
        return OrderedDict([
            ('id', user.id),
            ('name', user.username),
            ('friendship', friendship)])
