from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import force_authenticate

from rest_api import views
from test.TestBase import TestBase

User = get_user_model()


class UserViewTestCase(TestBase):

    def setUp(self):
        super().setUp()

    def tearDown(self) -> None:
        super().tearDown()

    def test_username_update(self):
        new_username = "jlaw"
        request = self.factory.patch('/username_update/', data={
            "username": new_username
        })
        force_authenticate(request, user=self.user1)

        response = views.username_update(request)

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        user = User.objects.filter(username=new_username).first()
        self.assertEqual(new_username, user.username)
        self.assertTrue(user.username_updated)

    def test_username_update_username_already_exists(self):
        new_username = self.user2.username
        request = self.factory.patch('/username_update/', data={
            "username": new_username
        })
        force_authenticate(request, user=self.user1)

        response = views.username_update(request)

        self.assertEqual(status.HTTP_226_IM_USED, response.status_code)
        user = User.objects.filter(username=self.user1.username).first()
        self.assertEqual(self.user1.username, user.username)
        self.assertFalse(user.username_updated)

    def test_username_update_bad_request(self):
        new_username = "jlaw"
        request = self.factory.patch('/username_update/', data={
            "junk": new_username
        })
        force_authenticate(request, user=self.user1)

        response = views.username_update(request)

        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
        self.assertEqual(self.user1.username, User.objects.filter(username=self.user1.username).first().username)

    def test_get_moments_not_signed_in(self):
        request = self.factory.get('/username_update/')

        self._assert_when_not_signed_in(lambda: views.username_update(request))
