import datetime
from datetime import timedelta
from unittest.mock import patch

from rest_framework import status
from rest_framework.test import force_authenticate

from latergator import settings
from rest_api import views
from rest_api.models import Moment
from test.TestBase import TestBase


class MomentViewTestCase(TestBase):

    def setUp(self):
        super().setUp()

    def test_get_moments_success(self):
        request = self.factory.get('/moments/')
        force_authenticate(request, user=self.user1)

        response = views.moment_preview_list(request)

        response_data = response.data
        self.assertEqual(2, len(response_data))

        moment_0 = response_data[0]
        self.assertEqual(self.moment2.id, moment_0["id"])
        self.assertEqual(2, len(moment_0["photos"]))
        moment_1 = response_data[1]
        self.assertEqual(4, len(moment_1["photos"]))
        self.assertEqual(self.moment1.id, moment_1["id"])

    def test_get_moments_none(self):
        request = self.factory.get('/moments/')
        force_authenticate(request, user=self.user2)

        response = views.moment_preview_list(request)

        self.assertEqual(0, len(response.data))

    def test_get_moments_not_signed_in(self):
        request = self.factory.get('/moments/')

        self._assert_when_not_signed_in(lambda: views.moment_preview_list(request))

    # def test_get_moments_delta_success(self):
    #     request = self.factory.get('/moments_delta/',
    #                                {'timeLastFetch': self.before_moment2_created.timestamp()})
    #     force_authenticate(request, user=self.user1)
    #
    #     response = views.moment_preview_list_delta(request)
    #
    #     response_data = response.data
    #     self.assertEqual(1, len(response_data))
    #
    #     moment_0 = response_data[0]
    #     self.assertEqual(2, len(moment_0["photos"]))
    #     self.assertEqual(self.photo5.id, moment_0["photos"][0]["id"])
    #     self.assertEqual(self.photo6.id, moment_0["photos"][1]["id"])
    #     self.assertEqual(self.moment2.id, moment_0["id"])

    def test_get_moments_delta_success_only_1_photo(self):
        request = self.factory.get('/moments_delta/',
                                   {'timeLastFetch': self.before_photo6_created.timestamp()})
        force_authenticate(request, user=self.user1)

        response = views.moment_preview_list_delta(request)

        response_data = response.data
        self.assertEqual(1, len(response_data))

        moment_0 = response_data[0]
        self.assertEqual(1, len(moment_0["photos"]))
        self.assertEqual(self.photo6.id, moment_0["photos"][0]["id"])
        self.assertEqual(self.moment2.id, moment_0["id"])

    def test_get_moments_delta_none(self):
        request = self.factory.get('/moments_delta/',
                                   {'timeLastFetch': self.before_moment2_created.timestamp()})
        force_authenticate(request, user=self.user2)

        response = views.moment_preview_list_delta(request)

        self.assertEqual(0, len(response.data))

    def test_get_moments_delta_not_signed_in(self):
        request = self.factory.get('/moments_delta/',
                                   {'time_last_fetch': self.moment2.created+timedelta(seconds=1)})

        self._assert_when_not_signed_in(lambda: views.moment_preview_list_delta(request))

    def test_get_moments_for_choosing_success(self):
        request = self.factory.get('/moments_for_choosing/')
        force_authenticate(request, user=self.user1)

        response = views.moments_for_choosing(request)

        response_data = response.data
        self.assertEqual(2, len(response_data))

        moment_1 = response_data[0]
        moment_2 = response_data[1]
        self.assertEqual(self.moment1.name, moment_1["name"])
        self.assertEqual(self.moment1.id, moment_1["id"])
        self.assertEqual(self.moment2.name, moment_2["name"])
        self.assertEqual(self.moment2.id, moment_2["id"])

    def test_get_moments_for_choosing_none(self):
        request = self.factory.get('/moments_for_choosing/')
        force_authenticate(request, user=self.user2)

        response = views.moments_for_choosing(request)

        self.assertEqual(0, len(response.data))

    def test_get_moments_for_choosing_not_signed_in(self):
        request = self.factory.get('/moments_for_choosing/')

        self._assert_when_not_signed_in(lambda: views.moments_for_choosing(request))

    def test_create_moment_success(self):
        request = self.factory.post(self.moment_create_path, data=self.new_data, format='json')
        force_authenticate(request, user=self.user1)

        response = views.moment_create(request)

        returned_data = response.data
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(self.num_moments_before+1, Moment.objects.count())
        self.assertEqual(self.new_name_2, returned_data['name'])
        self.assertEqual(self._format(self.datetime2), returned_data['publishDatetime'])

        returned_data_followers = returned_data['followers']
        self.assertEqual(2, len(returned_data_followers))
        self.assertEqual([self.user1.id, self.user2.id], returned_data_followers)

    def test_create_moment_not_signed_in(self):
        request = self.factory.post(self.moment_create_path, data=self.new_data)

        self._assert_when_not_signed_in(lambda: views.moment_create(request))
        self.assertEqual(self.num_moments_before, Moment.objects.count())

    def test_get_moment_success(self):
        request = self.factory.get(self._get_path_to_moment1())
        force_authenticate(request, user=self.user1)

        response = views.moment_detail(request, self.moment1.id)

        data = response.data
        self.assertEqual(self.moment1.name, data['name'])
        self.assertEqual(self.moment1.id, data['id'])
        self.assertEqual(self._format(self.moment1.publish_datetime), data['publishDatetime'])
        self.assertEqual(4, len(data['photos']))

    def _format(self, datetime):
        return datetime.strftime(settings.REST_FRAMEWORK.get('DATETIME_FORMAT'))

    def test_get_moment_not_allowed(self):
        request = self.factory.get(self._get_path_to_moment1())
        view_call = lambda: views.moment_detail(request, self.moment1.id)

        self._assert_not_allowed(request, view_call)

    def test_put_moment_success(self):
        new_data = {
            "name": self.new_name_1,
        }
        request = self.factory.patch(self._get_path_to_moment1(), data=new_data)
        force_authenticate(request, user=self.user1)

        response = views.moment_detail(request, self.moment1.id)

        returned_data = response.data
        self.assertEqual(self.new_name_1, returned_data["name"])

    def test_put_moment_not_allowed(self):
        request = self.factory.patch(self._get_path_to_moment1())
        view_call = lambda: views.moment_detail(request, self.moment1.id)

        self._assert_not_allowed(request, view_call)

    def test_delete_moment_success(self):
        request = self.factory.delete(self._get_path_to_moment1())
        force_authenticate(request, user=self.user1)

        response = views.moment_detail(request, self.moment1.id)

        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        self.assertEqual(1, Moment.objects.count())

    def test_delete_moment_not_allowed(self):
        request = self.factory.delete(self._get_path_to_moment1())
        view_call = lambda: views.moment_detail(request, self.moment1.id)

        self._assert_not_allowed(request, view_call)
