from unittest import TestCase

from django.contrib.auth import get_user_model


class UsersManagersTests(TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.username1 = 'john'
        self.email1 = 'j@l.com'

    def test_create_user(self):
        User = get_user_model()
        user = User.objects.create_user(self.username1, email=self.email1)
        self.assertEqual(user.username, self.username1)
        self.assertEqual(user.email, self.email1)
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)
        with self.assertRaises(TypeError):
            User.objects.create_user()
        with self.assertRaises(ValueError):
            User.objects.create_user('')
        with self.assertRaises(ValueError):
            User.objects.create_user(username='', password="foo")
