from datetime import timedelta
from unittest.mock import patch

from rest_framework import status
from rest_framework.test import force_authenticate

from latergator import settings
from rest_api import views
from rest_api.models import Moment
from test.TestBase import TestBase


class MomentViewTestCase(TestBase):

    def setUp(self):
        super().setUp()

        self.current_moment_path = 'current_moment/'
        self.new_current_moment = {
            'id': self.moment2.id
        }

    def test_get_current_moment_success(self):
        request = self.factory.get(self.current_moment_path)
        force_authenticate(request, user=self.user1)

        response = views.current_moment(request)

        response_data = response.data
        self.assertEqual(1, len(response_data))
        self.assertEqual(self.moment1.id, response_data['current_moment_id'])

    def test_get_moments_none(self):
        request = self.factory.get(self.current_moment_path)
        force_authenticate(request, user=self.user2)

        response = views.current_moment(request)

        self.assertEqual(None, response.data.get('current_moment_id'))

    def test_get_moments_not_signed_in(self):
        request = self.factory.get(self.current_moment_path)

        self._assert_when_not_signed_in(lambda: views.current_moment(request))

    def test_update_current_moment_success(self):
        request = self.factory.patch(self.current_moment_path, data=self.new_current_moment)
        force_authenticate(request, user=self.user1)

        response = views.current_moment(request)

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.user1.refresh_from_db()
        self.assertEqual(self.moment2, self.user1.current_moment)

    def test_update_moments_not_signed_in(self):
        request = self.factory.patch(self.current_moment_path)

        self._assert_when_not_signed_in(lambda: views.current_moment(request))
