from rest_framework import status
from rest_framework.test import force_authenticate

from rest_api import views
from rest_api.models import Photo
from test.TestBase import TestBase


class PhotoViewTestCase(TestBase):

    def setUp(self):
        super().setUp()

        self.image1 = open("test/media/photos/thx.jpg", 'rb')
        self.new_data_photo = {
            "image": self.image1
        }
        self.request_post_photo = self.factory.post(
            "/moments_photo_create/" + str(self.moment1.id),
            data=self.new_data_photo,
            format='multipart')
        self.request_delete_photo = self.factory.delete("/photo_detail/" + str(self.photo4.id))

        self.num_photos_before = Photo.objects.count()

    def tearDown(self) -> None:
        super().tearDown()
        self.image1.close()

    def test_post_photo_success(self):
        force_authenticate(self.request_post_photo, user=self.user1)

        response = views.moment_detail_photo_create(self.request_post_photo, self.moment1.id)

        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(self.num_photos_before_moment_1+1, self.moment1.photos.count())

    def test_post_photo_denied_access_to_moment(self):
        force_authenticate(self.request_post_photo, user=self.user2)

        view_call = lambda: views.moment_detail_photo_create(self.request_post_photo, self.moment1.id)

        self._assert_not_allowed(self.request_post_photo, view_call)
        self.assertEqual(self.num_photos_before, Photo.objects.count())

    def test_post_photo_not_signed_in(self):
        self._assert_when_not_signed_in(lambda: views.moment_detail_photo_create(self.request_post_photo, self.moment1.id))
        self.assertEqual(self.num_photos_before, Photo.objects.count())

    def test_delete_photo_success(self):
        force_authenticate(self.request_delete_photo, user=self.user1)

        response = views.photo_detail(self.request_delete_photo, self.photo4.id)

        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        self.assertEqual(self.num_photos_before-1, Photo.objects.count())

    def test_delete_photo_denied_access_to_delete(self):
        force_authenticate(self.request_delete_photo, user=self.user2)

        view_call = lambda: views.photo_detail(self.request_delete_photo, self.photo4.id)

        self._assert_not_allowed(self.request_post_photo, view_call)
        self.assertEqual(self.num_photos_before, Photo.objects.count())

    def test_delete_photo_not_signed_in(self):

        self._assert_when_not_signed_in(lambda: views.photo_detail(self.request_delete_photo, self.photo4.id))
        self.assertEqual(self.num_photos_before, Photo.objects.count())

    def test_authorized_for_photo_success(self):
        header = {
            "HTTP_X-Original-Uri": "/media/" + str(self.photo4.image)
        }
        request_authorized_for_photo = self.factory.get("/authorized_for_photo/", **header)
        force_authenticate(request_authorized_for_photo, user=self.user1)

        response = views.authorized_for_photo(request_authorized_for_photo)

        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_authorized_for_photo_denied(self):
        header = {
            "HTTP_X-Original-Uri": "/media/" + str(self.photo4.image)
        }
        request_authorized_for_photo = self.factory.get("/authorized_for_photo/", **header)
        force_authenticate(request_authorized_for_photo, user=self.user2)

        response = views.authorized_for_photo(request_authorized_for_photo)

        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)

    def test_authorized_for_photo_error(self):

        header = {
            "HTTP_X-Original-Uri": "/media/" + "no-photo-exists"
        }
        request_authorized_for_photo = self.factory.get("/authorized_for_photo/", **header)
        force_authenticate(request_authorized_for_photo, user=self.user1)

        response = views.authorized_for_photo(request_authorized_for_photo)

        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
