from datetime import datetime, timezone, timedelta
from typing import Callable

from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.exceptions import ErrorDetail
from rest_framework.response import Response
from rest_framework.test import APIRequestFactory, force_authenticate, APITransactionTestCase

from latergator import settings
from rest_api.models import Moment, Photo, Friendship, FriendshipStatus

NO_AUTH_CREDS = {
    'detail': ErrorDetail(string='Authentication credentials were not provided.', code='not_authenticated')
}

User = get_user_model()


class TestBase(APITransactionTestCase):

    def setUp(self):
        self.moment_create_path = 'moment_create'
        self.new_name_1 = "Ski Trip 2020"
        self.new_name_2 = "Xmas"
        self.datetime1 = datetime(year=2021, month=1, day=1, hour=1, minute=1, tzinfo=timezone.utc)
        self.datetime2 = datetime(year=2022, month=2, day=2, hour=2, minute=2, tzinfo=timezone.utc)
        self.datetime2_epoch = datetime(year=2022, month=2, day=2, hour=2, minute=2, tzinfo=timezone.utc).timestamp()

        self.user1 = User.objects.create(username="lawlor")
        self.user2 = User.objects.create(username="medugno")
        self.user3 = User.objects.create(username="beau")
        self.user4 = User.objects.create(username="jacob")
        self.user5 = User.objects.create(username="apple")
        self.user6 = User.objects.create(username="joey")
        self.user7 = User.objects.create(username="junior")
        self.user8 = User.objects.create(username="eight")
        self.user9 = User.objects.create(username="josephina")
        self.user10 = User.objects.create(username="joseph")
        self.user11 = User.objects.create(username="joel")

        # self.friendship1 = Friendship.objects.create(
        #     from_user=self.user1,
        #     to_user=self.user2,
        #     status=FriendshipStatus.ACCEPTED.value,
        # )
        #
        # self.friendship2 = Friendship.objects.create(
        #     from_user=self.user1,
        #     to_user=self.user10,
        #     status=FriendshipStatus.ACCEPTED.value,
        # )

        self.moment1 = Moment.objects.create(name="Ithaca", publish_datetime=self.datetime1)
        self.moment1.followers.set([self.user1])
        self.moment1.created = self.datetime1
        self.moment1.save()
        self.moment2 = Moment.objects.create(name="Xmas", publish_datetime=self.datetime2)
        self.moment2.followers.set([self.user1])
        self.moment2.created = self.datetime2
        self.moment2.save()

        self.before_moment2_created = self.moment2.created - timedelta(seconds=1)

        self.user1.current_moment = self.moment1
        self.user1.save()
        self.current_moment1 = self.user1.current_moment

        self.image_path_1 = "photos/image_path_1"
        self.image_path_4 = "photos/image_path_4"
        self.photo1 = Photo.objects.create(owner=self.user1, moment=self.moment1, image=self.image_path_1)
        self.photo2 = Photo.objects.create(owner=self.user2, moment=self.moment1, image=self.image_path_1)
        self.photo3 = Photo.objects.create(owner=self.user2, moment=self.moment1, image=self.image_path_1)
        self.photo4 = Photo.objects.create(owner=self.user1, moment=self.moment1, image=self.image_path_4)

        self.photo5 = Photo.objects.create(owner=self.user1, moment=self.moment2, image=self.image_path_1)
        self.photo6 = Photo.objects.create(owner=self.user1, moment=self.moment2, image=self.image_path_1)
        self.photo5.created = self.datetime2 + timedelta(minutes=2)
        self.photo5.save()
        self.photo6.created = self.datetime2 + timedelta(minutes=4)
        self.photo6.save()

        self.before_photo6_created = self.datetime2 + timedelta(minutes=3)

        self.new_data = {
            "name": self.new_name_2,
            "publishDatetime": self.datetime2.strftime(settings.REST_FRAMEWORK.get('DATETIME_FORMAT')),
            "followers": [self.user2.id]
        }

        self.num_moments_before = Moment.objects.count()
        self.num_photos_before_moment_1 = self.moment1.photos.count()

        self.factory = APIRequestFactory()

    def _get_path_to_moment1(self):
        return '/moments/' + str(self.moment1.id)

    def _assert_not_allowed(self, request, view_call: Callable[[], Response]):
        force_authenticate(request, user=self.user2)

        response = view_call()

        expected_response = {'detail': ErrorDetail(string='You do not have permission to perform this action.', code='permission_denied')}
        self.assertEqual(expected_response, response.data)

    def _assert_when_not_signed_in(self, view_call: Callable[[], Response]):
        response = view_call()

        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)
        self.assertEqual(NO_AUTH_CREDS, response.data)
