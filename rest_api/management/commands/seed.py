import logging
from datetime import datetime, timezone

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from rest_api.models import Moment, Photo, Friendship, FriendshipStatus

logger = logging.getLogger(__name__)

MODE_REFRESH = 'refresh'
MODE_CLEAR = 'clear'

User = get_user_model()


class Command(BaseCommand):
    help = "seed database for testing and development."

    def add_arguments(self, parser):
        parser.add_argument('--mode', type=str, choices=[MODE_REFRESH, MODE_CLEAR], help="Mode")

    def handle(self, *args, **options):
        logger.info('Seeding data...')
        run_seed(self, options['mode'])
        logger.info('Done.')


def clear_data():
    """Deletes all the table data"""
    logger.info("Deleting User instances")
    User.objects.all().delete()
    Moment.objects.all().delete()
    Photo.objects.all().delete()

def create_users():
    """Creates an address object combining different elements from the list"""
    logger.info("Creating users")
    password = "asdfasdf"
    users_info = [
        ("lawlor", password),
        ("jacob", password),
        ("will", password),
        ("jones", password),
        ("winston", password),
        ("jane", password),
        ("jabe", password),
    ]

    for user_info in users_info:
        user = User.objects.create_user(
            username=user_info[0],
        )
        user.set_password(user_info[1])
        user.save()
        logger.info("{} address created.".format(user))

def create_moments():
    user1 = User.objects.filter(username='lawlor').first()
    moment1 = Moment.objects.create(name="Ithaca Ski Trip", publish_datetime=datetime.now(timezone.utc))
    moment1.followers.set([user1])
    moment1.save()
    photo1 = Photo.objects.create(owner=user1, moment=moment1, image="photos/ski1@3x.jpg")
    photo2 = Photo.objects.create(owner=user1, moment=moment1, image="photos/ski2@3x.jpg")
    photo3 = Photo.objects.create(owner=user1, moment=moment1, image="photos/ski3@3x.jpg")
    photo4 = Photo.objects.create(owner=user1, moment=moment1, image="photos/ski4@3x.jpg")
    photo5 = Photo.objects.create(owner=user1, moment=moment1, image="photos/ski5@3x.jpg")
    photo7 = Photo.objects.create(owner=user1, moment=moment1, image="photos/ski7@3x.jpg")

    moment2 = Moment.objects.create(name="Christmas 2020", publish_datetime=datetime.now(timezone.utc))
    moment2.followers.set([user1])
    moment2.save()
    photo1 = Photo.objects.create(owner=user1, moment=moment2, image="photos/xmas1@3x.jpg")
    photo2 = Photo.objects.create(owner=user1, moment=moment2, image="photos/xmas2@3x.jpg")
    photo3 = Photo.objects.create(owner=user1, moment=moment2, image="photos/xmas3@3x.jpg")
    photo4 = Photo.objects.create(owner=user1, moment=moment2, image="photos/xmas4@3x.jpg")
    photo5 = Photo.objects.create(owner=user1, moment=moment2, image="photos/xmas5@3x.jpg")

    moment3 = Moment.objects.create(name="Thanksgiving 2020", publish_datetime=datetime.now(timezone.utc))
    moment3.followers.set([user1])
    moment3.save()
    photo1 = Photo.objects.create(owner=user1, moment=moment3, image="photos/thx1@3x.jpg")
    photo2 = Photo.objects.create(owner=user1, moment=moment3, image="photos/thx2@3x.jpg")
    photo3 = Photo.objects.create(owner=user1, moment=moment3, image="photos/thx3@3x.jpg")
    photo4 = Photo.objects.create(owner=user1, moment=moment3, image="photos/thx4@3x.jpg")
    photo5 = Photo.objects.create(owner=user1, moment=moment3, image="photos/thx5@3x.jpg")

def create_friendships():
    user1 = User.objects.filter(username='lawlor').first()
    user2 = User.objects.filter(username='winston').first()
    user3 = User.objects.filter(username='jacob').first()
    user4 = User.objects.filter(username='jane').first()
    user5 = User.objects.filter(username='jabe').first()
    Friendship.objects.create(from_user=user1, to_user=user2, status=FriendshipStatus.ACCEPTED.value)
    Friendship.objects.create(from_user=user1, to_user=user3, status=FriendshipStatus.ACCEPTED.value)
    Friendship.objects.create(from_user=user1, to_user=user4, status=FriendshipStatus.REQUESTED.value)
    Friendship.objects.create(from_user=user1, to_user=user5, status=FriendshipStatus.DENIED.value)

def run_seed(self, mode):
    """ Seed database based on mode

    :param mode: refresh / clear
    :return:
    """
    # Clear data from tables
    clear_data()
    if mode == MODE_CLEAR:
        return

    create_users()
    create_moments()
    create_friendships()