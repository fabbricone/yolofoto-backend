from django.contrib.auth import get_user_model
from django.http import Http404, QueryDict
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from rest_api.models import Friendship, FRIENDSHIP_STATUS_CHOICES, FriendshipStatus
from rest_api.permissions import IsFriendInFrienship
from rest_api.serializers import FriendshipSerializer, UserSerializer, FindFriendSerializer

User = get_user_model()


class FriendList(APIView):
    """
    Get existing friends (for new moment who view and friends view)
    """
    def get(self, request):
        current_user = request.user
        friendship_status_str = request.query_params.get('status')
        if friendship_status_str == FriendshipStatus.ACCEPTED.value:
            friendship_status = FriendshipStatus.ACCEPTED
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        friends = Friendship.friends(current_user, friendship_status)
        serializer = UserSerializer(friends, many=True, context={'request': request._request})
        return Response(serializer.data)


class FriendshipList(APIView):

    param_prefix = openapi.Parameter('prefix',
                                     openapi.IN_QUERY,
                                     description="Prefix of a username of a user to search for.",
                                     type=openapi.TYPE_STRING)
    user_response = openapi.Response('Find friends.', UserSerializer(many=True, write_only="password"))

    @swagger_auto_schema(manual_parameters=[param_prefix], responses={200: user_response})
    def get(self, request):
        """
        Find future_friends
        """
        current_user = request.user
        request_query_params = request.query_params

        friendship_status_str = request_query_params.get('status')
        if friendship_status_str == FriendshipStatus.REQUESTED.value:
            future_friends = Friendship.friends(current_user, FriendshipStatus.REQUESTED)
        else:
            friends = Friendship.friends(current_user, FriendshipStatus.ACCEPTED)
            not_friends = Friendship.friends(current_user, FriendshipStatus.DENIED)
            users_except = User.objects \
                .exclude(username=current_user.username) \
                .exclude(username__in=[f.username for f in friends]) \
                .exclude(username__in=[f.username for f in not_friends])
            if 'prefix' in request_query_params.keys():
                prefix = request_query_params.get('prefix').lower()
                future_friends = users_except \
                                     .filter(username__startswith=prefix) \
                                     .order_by('username')[:10]
            else:
                future_friends = users_except.order_by('username')[:10]

        context = {
            'request': request._request,
            'current_user': current_user,
        }
        serializer = FindFriendSerializer(future_friends, many=True, context=context)
        return Response(serializer.data)

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'from_user': openapi.Schema(type=openapi.TYPE_INTEGER, description='The id of the "from" user in the friendship'),
            'to_user': openapi.Schema(type=openapi.TYPE_INTEGER, description='The id of the "to" user in the friendship'),
        }))
    def post(self, request):
        """
        Create a friendship
        """
        data = QueryDict.dict(request.data)
        if 'to_user' in data.keys():
            data['from_user'] = request.user.id
        serializer = FriendshipSerializer(data=data, context={'request': request._request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FriendshipDetail(APIView):
    permission_classes = [IsAuthenticated, IsFriendInFrienship]

    def get_object(self, pk):
        try:
            obj = Friendship.objects.get(pk=pk)
        except Friendship.DoesNotExist:
            raise Http404
        self.check_object_permissions(self.request, obj)
        return obj

    # def get(self, request, pk):
    #     friendship = self.get_object(pk)
    #     serializer = FriendshipSerializer(friendship)
    #     return Response(serializer.data)

    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'from_user': openapi.Schema(type=openapi.TYPE_INTEGER, description='The id of the "from" user in the friendship'),
            'to_user': openapi.Schema(type=openapi.TYPE_INTEGER, description='The id of the "to" user in the friendship'),
            'status': openapi.Schema(type=openapi.TYPE_STRING, description='The new status of the frienship', enum=[f[0] for f in FRIENDSHIP_STATUS_CHOICES]),
        }))
    def patch(self, request, pk):
        friendship = self.get_object(pk)
        serializer = FriendshipSerializer(friendship, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # def delete(self, request, pk):
    #     friendship = self.get_object(pk)
    #     friendship.delete()
    #     return Response(status=status.HTTP_204_NO_CONTENT)
