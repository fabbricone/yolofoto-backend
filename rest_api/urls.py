from django.urls import path, re_path, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework.urlpatterns import format_suffix_patterns

from rest_api import views, views_friendship

# Create a router and register our viewsets with it.
# router = DefaultRouter()
# router.register(r'moments', views.MomentViewSet)
# router.register(r'photos', views.PhotoViewSet)
# router.register(r'friendship', views.FriendshipViewSet)
# router.register(r'users', views.UserViewSet)
#
# # The API URLs are now determined automatically by the router.
# urlpatterns = [
#     path('', include(router.urls)),
# ]

schema_view = get_schema_view(
    openapi.Info(
        title="LaterGator API",
        default_version='v1',
        description="Welcome to the world of LaterGator",
        terms_of_service="https://www.latergator.org",
        contact=openapi.Contact(email="lateg@tor.org"),
        license=openapi.License(name="Request/Response"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    # re_path(r'^doc(spec\.json|\.yaml)$',
    #         schema_view.without_ui(cache_timeout=0), name='schema-json'),
    # path('doc/', schema_view.with_ui('swagger', cache_timeout=0),
    #      name='schema-swagger-ui'),
    # path('redoc/', schema_view.with_ui('redoc', cache_timeout=0),
    #      name='schema-redoc'),
    path('still_authenticated/', views.still_authenticated),
    path('authorized_for_photo/', views.authorized_for_photo),
    path('moments/', views.moment_preview_list),
    path('moments_delta/', views.moment_preview_list_delta),
    path('moments_for_choosing/', views.moments_for_choosing),
    path('moment_create/', views.moment_create),
    path('moments/<int:pk>', views.moment_detail, name="moment-detail"),
    path('moments_photo_create/<int:pk>', views.moment_detail_photo_create),
    path('current_moment/', views.current_moment),
    path('photos/<int:pk>', views.photo_detail, name="photo-detail"),
    path('users/', views.UserList.as_view()),
    path('users/<int:pk>/', views.UserDetail.as_view(), name="customuser-detail"),
    path('username_update/', views.username_update),
    path('friends/', views_friendship.FriendList.as_view()),
    path('friendships/', views_friendship.FriendshipList.as_view()),
    path('friendships/<int:pk>', views_friendship.FriendshipDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)