from enum import Enum

from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils import timezone

from rest_api.managers import CustomUserManager


class FriendshipStatus(Enum):
    REQUESTED = 'requested'
    ACCEPTED = 'accepted'
    DENIED = 'denied'

FRIENDSHIP_STATUS_CHOICES = [
    ('no_relationship', 'No relationship'),
    (FriendshipStatus.REQUESTED.value, 'Requested'),
    (FriendshipStatus.ACCEPTED.value, 'Accepted'),
    (FriendshipStatus.DENIED.value, 'Denied'),
    # ('blocked', 'Blocked'),
]


class Friendship(models.Model):
    from_user = models.ForeignKey('CustomUser', on_delete=models.CASCADE, related_name='user1')
    to_user = models.ForeignKey('CustomUser', on_delete=models.CASCADE, related_name='user2')
    status = models.CharField(choices=FRIENDSHIP_STATUS_CHOICES, default=FriendshipStatus.REQUESTED.value, max_length=100)

    @staticmethod
    def friends(user, friendship_status: FriendshipStatus):
        friends_to = [friendship.to_user for friendship in Friendship.objects
            .filter(from_user=user, status=friendship_status.value)]
        friends_from = [friendship.from_user for friendship in Friendship.objects
            .filter(to_user=user, status=friendship_status.value)]
        return sorted(friends_from + friends_to, key=lambda user: user.username)


class Moment(models.Model):
    name = models.CharField(max_length=100, blank=True, default='')
    publish_datetime = models.DateTimeField()
    followers = models.ManyToManyField('CustomUser', related_name='moments', through='Moment_Followers')
    created = models.DateTimeField(auto_now_add=True)


class Moment_Followers(models.Model):
    follower = models.ForeignKey('CustomUser', on_delete=models.CASCADE)
    moment = models.ForeignKey(Moment, on_delete=models.CASCADE)
    is_current_moment = models.BooleanField(default=False)


class Photo(models.Model):
    class Meta:
        ordering = ['created']

    owner = models.ForeignKey('CustomUser', related_name='photos', on_delete=models.CASCADE)
    moment = models.ForeignKey(Moment, related_name='photos', on_delete=models.CASCADE)
    image = models.ImageField(upload_to='photos')
    created = models.DateTimeField(auto_now_add=True)


class CustomUser(AbstractBaseUser, PermissionsMixin):
    class Meta:
        ordering = ['username']

    username = models.TextField(unique=True)
    email = models.TextField(unique=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)
    current_moment = models.ForeignKey(Moment, on_delete=models.CASCADE, null=True)
    username_updated = models.BooleanField(default=False)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.username