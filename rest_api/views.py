# Create your views here.
import datetime

from django.contrib.auth import get_user_model
from django.db import IntegrityError
from django.http import QueryDict
from django.utils import timezone
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status, generics
from rest_framework.decorators import api_view, parser_classes
from rest_framework.exceptions import PermissionDenied
from rest_framework.parsers import MultiPartParser
from rest_framework.request import Request
from rest_framework.response import Response
from django.conf import settings

from rest_api.models import Moment, Photo
from rest_api.serializers import UserSerializer, PhotoSerializer, MomentCreateSerializer, \
    MomentDetailSerializer, MomentForChoosingSerializer, CurrentMomentSerializer


# We should refactor these into Class-based views
@api_view(['GET'])
def still_authenticated(request: Request):
    return Response(status=status.HTTP_200_OK)

@api_view(['GET'])
def authorized_for_photo(request: Request):
    try:
        full_photo_url = request.headers.get('X-Original-Uri')
        database_photo_url = full_photo_url.split(settings.MEDIA_URL)[1]
        photo = Photo.objects.filter(image=database_photo_url)
        if request.user in photo.first().moment.followers.all():
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_401_UNAUTHORIZED)
    except:
        return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def moment_preview_list(request: Request):
    moments = Moment.objects \
        .filter(followers__in=[request.user]) \
        .order_by('-publish_datetime')
    serializer = MomentDetailSerializer(moments, many=True, context={'request': request._request})
    return Response(serializer.data)

@api_view(['GET'])
def moment_preview_list_delta(request: Request):
    time_last_fetch_epoch = request.query_params.get('timeLastFetch')
    time_last_fetch = datetime.datetime.fromtimestamp(int(float(time_last_fetch_epoch)), timezone.utc)
    moments = Moment.objects \
        .filter(followers__in=[request.user]) \
        .filter(created__gt=time_last_fetch) \
        .order_by('-publish_datetime')

    moments_all_for_user = Moment.objects \
        .filter(followers__in=[request.user])

    photos = Photo.objects \
        .filter(moment__in=moments_all_for_user) \
        .filter(created__gt=time_last_fetch)

    moments_with_new_photos = set([photo.moment for photo in photos])
    moments_all = list(moments) + list(moments_with_new_photos)

    context = {
        'request': request._request,
        'time_last_fetch': time_last_fetch,
    }

    serializer = MomentDetailSerializer(moments_all, many=True, context=context)
    return Response(serializer.data)

# We should refactor these into Class-based views
@api_view(['GET'])
def moments_for_choosing(request: Request):
    moments = Moment.objects.filter(followers__in=[request.user])
    serializer = MomentForChoosingSerializer(moments, many=True, context={'request': request._request})
    return Response(serializer.data)

# We should refactor these into Class-based views
@api_view(['POST'])
def moment_create(request: Request):
    user = request.user
    data = QueryDict.dict(request.data)
    data['followers'] = data.get('followers') + [str(user.id)]
    serializer = MomentCreateSerializer(data=data, context={'request': request._request})
    if serializer.is_valid():
        moment = serializer.save()
        user.current_moment = moment
        user.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# We should refactor these into Class-based views
@api_view(['GET', 'PATCH', 'DELETE'])
def moment_detail(request, pk):
    """
    Retrieve, update or delete a moment.
    """

    try:
        moment = Moment.objects.get(pk=pk)
    except Moment.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    user_transgression = user_in_violation(request.user not in moment.followers.all())
    if user_transgression is not None:
        return user_transgression

    context = {'request': request._request}
    if request.method == 'GET':
        serializer = MomentDetailSerializer(moment, context=context)
        return Response(serializer.data)

    # elif request.method == 'POST':
    #     data = request.data
    #     data['owner'] = request.user.username
    #     data['image'] = request.data['file']
    #     serializer = PhotoSerializer(data=request.data, context=context)
    #     if serializer.is_valid():
    #         serializer.save(owner=request.user, moment=moment)
    #         return Response(status=status.HTTP_201_CREATED)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'PATCH':
        serializer = MomentDetailSerializer(moment, data=request.data, partial=True, context=context)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        moment.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

# We should refactor these into Class-based views
@api_view(['GET', 'PATCH'])
def current_moment(request: Request):
    current_moment = request.user.current_moment
    if request.method == 'GET':
        serializer = CurrentMomentSerializer(current_moment)
        return Response(serializer.data)
    elif request.method == 'PATCH':
        new_current_moment_id = request.data.get("id")
        moment = Moment.objects.filter(id=new_current_moment_id).first()
        if moment:
            request.user.current_moment = moment
            request.user.save()
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)

def get_time_now():
    return timezone.now()

id = openapi.Parameter('id',
                              required=True,
                              in_='path',
                              description="Id of the moment to which you are posting a photo.",
                              type=openapi.TYPE_INTEGER)
@swagger_auto_schema(methods=['post'], manual_parameters=[id], responses={201: ''})
@api_view(['POST'])
@parser_classes([MultiPartParser])
def moment_detail_photo_create(request, pk):
    try:
        moment = Moment.objects.get(pk=pk)
    except Moment.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    user_transgression = user_in_violation(request.user not in moment.followers.all())
    if user_transgression is not None:
        return user_transgression

    context = {'request': request._request}

    data = {}
    data['owner'] = request.user.username
    # todo we send an image url to the image, so that's what we call it in the serializer
    data['imageUrl'] = request.data['image']
    serializer = PhotoSerializer(data=data, context=context)
    if serializer.is_valid():
        serializer.save(owner=request.user, moment=moment)
        return Response(status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'DELETE'])
def photo_detail(request, pk):
    try:
        photo = Photo.objects.get(pk=pk)
    except Photo.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    user_transgression = user_in_violation(request.user != photo.owner)
    if user_transgression is not None:
        return user_transgression

    context = {'request': request._request}
    if request.method == 'GET':
        """
        Get a photo.
        """
        serializer = PhotoSerializer(photo, context=context)
        return Response(serializer.data)

    elif request.method == 'DELETE':
        """
        Delete a photo.
        """
        photo.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

def user_in_violation(in_violation: bool):
    if in_violation:
        raise PermissionDenied()
    return None


class UserList(generics.ListAPIView):
    User = get_user_model()
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    User = get_user_model()
    queryset = User.objects.all()
    serializer_class = UserSerializer

@api_view(['PATCH'])
def username_update(request: Request):
    try:
        current_user = request.user
        username = request.data["username"]
        current_user.username = username
        current_user.username_updated = True
        current_user.save()
        return Response(data={ 'username': username }, status=status.HTTP_200_OK,)
    except IntegrityError as e:
        return Response(status=status.HTTP_226_IM_USED)
    except Exception as e:
        return Response(status=status.HTTP_400_BAD_REQUEST)
