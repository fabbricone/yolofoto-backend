from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.fields import SerializerMethodField
from rest_framework.relations import PrimaryKeyRelatedField

from rest_api.models import Moment, Photo, Friendship, CustomUser

User = get_user_model()


class PhotoSerializer(serializers.HyperlinkedModelSerializer):
    imageUrl = serializers.ImageField(max_length=None, source="image")
    owner = serializers.CharField()

    class Meta:
        model = Photo
        fields = ['id', 'owner', 'created', 'imageUrl', ]


class UserSerializer(serializers.HyperlinkedModelSerializer):
    password = serializers.CharField(write_only=True)
    name = serializers.CharField(source='username')

    class Meta:
        model = User
        fields = ['url', 'id', 'name', "password", ]

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data['username'],
            password=validated_data['password'],
        )

        return user


class FindFriendSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='username')
    friendship = SerializerMethodField()

    class Meta:
        model = User
        fields = ['id', 'name', 'friendship', ]

    def get_friendship(self, obj):
        current_user = self.context.get("current_user")
        friendship = Friendship.objects.filter(from_user=obj, to_user=current_user).first()
        if not friendship:
            friendship = Friendship.objects.filter(from_user=current_user, to_user=obj).first()
        if not friendship:
            return None
        return FriendshipSerializer(friendship, context=self.context).data


class FriendshipSerializer(serializers.ModelSerializer):
    from_user = PrimaryKeyRelatedField(queryset=User.objects.all())
    to_user = PrimaryKeyRelatedField(queryset=User.objects.all())

    class Meta:
        model = Friendship
        fields = ['id', 'from_user', 'to_user', 'status', ]


class MomentDetailSerializer(serializers.HyperlinkedModelSerializer):
    followers = UserSerializer(read_only=True, many=True)
    publishDatetime = serializers.DateTimeField(source='publish_datetime')
    # photos = PhotoSerializer(read_only=True, many=True)
    photos = SerializerMethodField()

    class Meta:
        model = Moment
        fields = ['url', 'id', 'name', 'publishDatetime', 'created', 'followers', 'photos', ]

    def get_photos(self, obj):
        if "time_last_fetch" in self.context.keys():
            time_last_fetch = self.context.get("time_last_fetch")
            photos = Photo.objects.filter(moment=obj, created__gt=time_last_fetch).all()
        else:
            photos = Photo.objects.filter(moment=obj).all()

        return PhotoSerializer(photos, read_only=True, many=True, context=self.context).data


class MomentForChoosingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Moment
        fields = ['id', 'name']


class CurrentMomentSerializer(serializers.ModelSerializer):
    current_moment_id = serializers.IntegerField(source='id')

    class Meta:
        model = Moment
        fields = ['current_moment_id']


class MomentCreateSerializer(serializers.HyperlinkedModelSerializer):
    followers = PrimaryKeyRelatedField(many=True, queryset=User.objects.all())
    publishDatetime = serializers.DateTimeField(source='publish_datetime')

    class Meta:
        model = Moment
        fields = ['url', 'id', 'name', 'publishDatetime', 'created', 'followers', ]
